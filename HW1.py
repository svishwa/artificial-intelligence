from collections import defaultdict
from copy import deepcopy
import numpy as np
import time

row = 0
col = 0
maxi = -1
dct = defaultdict(list)
num_police = 0
num_scooters = 0
brd = list()
# l = 0
res_lst = list()
flag = 0


def input():
    global row, col, dct, num_scooters, num_police
    with open("/Users/sv/Downloads/input1.txt") as fp:
        lst = fp.readlines()
    lst = [x.strip() for x in lst]
    row = int(lst[0])
    col = int(lst[0])
    num_police = int(lst[1])
    num_scooters = int(lst[2])
    co_ord = lst[3:]
    co_ord = [x.split(',') for x in co_ord]
    j = 0
    k = 0
    while j < num_scooters:
        i = 0
        while i < 12:
            dct[j].append(co_ord[k])
            k += 1
            i += 1
        j += 1


def board():
    global brd
    brd = [[0 for i in xrange(col)] for j in xrange(row)]
    for k, v in dct.items():
        for l in v:
            brd[int(l[0])][int(l[1])] += 1


def handle(Matrix, r, c, p, tot):
    global row, col, maxi
    tmp = map(list, Matrix)
    j = c

    if r>=0:
        k=0
        while k<col:
            tmp[r-1][k]=float("-inf")
            k+=1
    for i in xrange(r, row, 1):
        if j < col:
            tmp[i][j] = float("-inf")
            j += 1

    i = r
    while i < row:
        tmp[i][c] = float("-inf")
        i += 1
    i = r
    while i >= 0:
        tmp[i][c] = float("-inf")
        i -= 1

    j = c
    for i in xrange(r, -1, -1):
        if j < col:
            tmp[i][j] = float("-inf")
            j += 1

    j = c
    while j < col:
        tmp[r][j] = float("-inf")
        j += 1

    j = c
    while j >= 0:
        tmp[r][j] = float("-inf")
        j -= 1

    j = c
    for i in xrange(r, row, 1):
        if j >= 0:
            tmp[i][j] = float("-inf")
            j -= 1

    j = c
    for i in xrange(r, -1, -1):
        if j >= 0:
            tmp[i][j] = float("-inf")
            j -= 1
    police(tmp, p - 1, tot, r)


def isSafe(i, j, check_lst):
    global row, col
    print "check:",i,j
    if len(check_lst) == 0:
        print "here"
        return True
    lst = list()
    for k in check_lst:
        r = int(k[0])
        c = int(k[1])
        n = c
        for m in xrange(row):
            lst.append(str(str(r) + str(m)))
            lst.append(str(str(m) + str(c)))
        for m in xrange(r, -1, -1):
            if n >= 0:
                lst.append(str(str(m)+ str(n)))
                n -= 1
        r = int(k[0])
        n = c
        for m in xrange(r, row, 1):
            if n >= 0:
                lst.append(str(str(m) + str(n)))
                n -= 1
        r = int(k[0])
        n = c
        for m in xrange(r, -1, -1):
            if n < col:
                lst.append(str(str(m) + str(n)))
                n += 1
        r = int(k[0])
        n = c
        for m in xrange(r, row, 1):
            if n < col:
                lst.append(str(str(m) + str(n)))
                n += 1

    if any(str(str(i) + str(j)) in s for s in lst):
        return False


def police(layout, p, tot, k, check_lst):
    global maxi, row, col, maxi, lst, flag
    i = 0
    if p >= 1:
        while i < row:
            if row - i < p:
                return
            for j in range(col):
                if i == k:
                    break
                if isSafe(i, j, check_lst):
                    print "police:",i, j
                    tot += layout[i][j]
                    check_lst.append(str(str(i) + str(j)))
                    print check_lst
                    police(layout, p, tot, i, check_lst)
                    check_lst.pop()
                    tot -= layout[i][j]
            i += 1
            check_lst.pop()
    else:
        return


if __name__ == "__main__":
    start = time.time()
    input()
    board()
    check_lst = list()
    layout = deepcopy(brd)
    police(layout, num_police, 0, -1, check_lst)
    if len(res_lst) != 0:
        maxi = max(res_lst)
    print maxi
    fp = open("/Users/sv/Desktop/output.txt", "w")
    if maxi == -1:
        fp.write("Police officers cant be placed because of collision")
    else:
        fp.write(str(int(maxi)))
    fp.close()
    for i in brd:
        print i
    end = time.time()
    print end - start
